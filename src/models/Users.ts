enum PreferredPayment {
    pix,
    creditCard,
    debit,
    crypto,
    other
};
export interface User {
    id: number;
    name: string;
    email: string;
    phone: string;
    address: string;
    complement: string;
    number: number;
    zipCode: string;
    neighborhood: string;
    preferredPayment: PreferredPayment;
    timestamp: Date;
};