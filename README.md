# Fabrica de Hamburguer - Backend Service - Cadastro de Cliente

## Objetivo:
Criar o cadastro do cliente na plataforma

## Approach
Criar a tabela users, setando as props
Criar o usuario, setando as props

## Stack
Node.js + Typescript

## ORM
TypeORM

## DB
PostgreSQL

## Container service
Docker