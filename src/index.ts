import "reflect-metadata";
import {createConnection} from "typeorm";
import {User} from "./entity/User";

const connection = createConnection()
    .then(async connection => {
        console.log("Iniciando a insercao de teste no DB...");
        
        const user = new User();
        user.name = "Homer Simpson";
        user.email = "homer@springfield.com";
        user.phone = 857362729183940;
        user.timestamp = Date.now();
        
        await connection.manager.save(user);
        console.log("Usuario criado. Id: " + user.id);

        console.log("Lendo usuarios da base...");
        const users = await connection.manager.find(User);
        console.log("Usuarios localizados: ", users);

    })
    .catch(error => console.log(error));

export default connection;