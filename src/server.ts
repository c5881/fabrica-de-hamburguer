import { server } from './app';
import { config } from 'dotenv';

config();

interface ICallback {
    (err?: any, result?: any):void
};

const callback:ICallback = (err,res) => {
    if(err){
        process.stdout.write(JSON.stringify(err))
    }else{
        process.stdout.write(JSON.stringify(res));
    };
};

server.listen(process.env.PORT,callback);