export interface Login {
    id: number;
    name: string;
    email: string;
    password: string;
    token: string;
    isLogged: boolean;
    timestamp: Date;
    metadata?: any;
};