import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";


enum PreferredPayment {
    pix,
    creditCard,
    debit,
    crypto,
    other
};
@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: 150
    })
    name: string;

    @Column({
        length: 200
    })
    email: string;

    @Column({
        length: 50
    })
    phone: number;

    @Column({
        length: 300
    })
    address: string;

    @Column({
        length: 100
    })
    complement: string;

    @Column({
        length: 20
    })
    number: number;

    @Column({
        length: 20
    })
    zipCode: string;

    @Column({
        length: 300
    })
    neighborhood: string;

    @Column('int')
    preferredPayment: PreferredPayment;

    @Column()
    timestamp: number;

}
