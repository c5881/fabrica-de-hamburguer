import { createServer,IncomingMessage,ServerResponse } from 'http';
import { URL, URLSearchParams } from 'url';
import connection from './index';

interface ICRProps{
    headers: any;
    method: any;
    body: string;
    query: URLSearchParams;
    path: string | null;
};

const acceptedContent = {
    contentDeclare: 'Content-type',
    mimeType: 'application/json'
};

const sendString = (chunkData: any): string => {
    return JSON.stringify(chunkData);
};

export const server = createServer((req: IncomingMessage,res: ServerResponse) => {
    
    if(['GET'].includes(req.method)){
        
        const mountedURI = `http://${req.headers.host}/`;
        
        const baseURL = new URL(mountedURI,'');
        
        const { pathname,searchParams } = baseURL;
        
        const { headers,method } = req;
        
        let incomingSerialized = '';
        
        req.setEncoding('utf-8');

        req.on('data', chunk => {
            incomingSerialized += chunk;
        });

        req.on('end', () => {
            incomingSerialized += null;

            let cr: ICRProps = {
                headers,
                method,
                body: incomingSerialized,
                query: searchParams,
                path: pathname
            };

            const pathInRoute = (path: string, target: IRouterProps):boolean => {
                if(Object.keys(target).includes(path)){
                    return true
                };
            };

            if(pathInRoute(cr.path,routes)){
                routes[cr.path](cr,res);
            }else{
                routes['notFound'](cr,res);
            };

        })

    }else{
        res.writeHead(405)
        res.end(sendString({message: 'Invalid http method. Check your request.'}))
    };

});

interface IRouterProps {
    [k:string]:any
};

const routes:IRouterProps = {
    ping: (cr: ICRProps,res: ServerResponse) => {
        res.setHeader(acceptedContent['contentDeclare'],acceptedContent['mimeType']);
        res.writeHead(200);
        res.end(sendString({message: 'Server listening successfully.'}));
    },
    notFound: (cr: ICRProps,res: ServerResponse) => {
        res.setHeader(acceptedContent['contentDeclare'],acceptedContent['mimeType']);
        res.writeHead(404);
        res.end(sendString({message: 'Path not found.'}));
    }
}